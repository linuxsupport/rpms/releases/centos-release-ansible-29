Name:           centos-release-ansible-29
Version:        1
Release:        2%{?dist}
Summary:        Ansible 2.9 packages from the CentOS ConfigManagement SIG repository

License:        MIT
URL:            https://wiki.centos.org/SpecialInterestGroup/ConfigManagementSIG/Ansible
Source0:        CentOS-SIG-ansible-29.repo
Requires:       centos-release-configmanagement
BuildArch:      noarch
Epoch:		666

%description
Ansible 2.9 packages as delivered via the CentOS ConfigManagement SIG.

%prep

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/yum.repos.d
install -Dpm0644 -t %{buildroot}%{_sysconfdir}/yum.repos.d %{S:0}

%files
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-SIG-ansible-29.repo

%changelog
* Fri Apr 03 2020 Ben Morrice <ben.morrice@cern.ch> - 1-2
- update for CERN (linuxsoft.cern.ch)

* Tue Mar 31 2020 Fabian Arrotin <arrfab@centos.org> - 1-1
- Initial package
